package simplifiedcoding.net.kotlinretrofittutorial.models

data class User(val userId:Int, val userName:String)
