package simplifiedcoding.net.kotlinretrofittutorial.models

data class LoginResponse(val errorCode: Int, val errorMessage:String, val user: User)