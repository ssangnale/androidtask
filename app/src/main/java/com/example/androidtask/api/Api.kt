package simplifiedcoding.net.kotlinretrofittutorial.api

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import simplifiedcoding.net.kotlinretrofittutorial.models.LoginResponse

interface Api {

    @FormUrlEncoded
    @POST("login")
    fun userLogin(
            @Field("username") email:String,
            @Field("password") password: String
    ):Call<LoginResponse>
}