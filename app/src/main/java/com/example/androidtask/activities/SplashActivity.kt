package com.example.androidtask.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.example.androidtask.BuildConfig
import com.example.androidtask.R
import kotlinx.android.synthetic.main.activity_splash.*
import simplifiedcoding.net.kotlinretrofittutorial.storage.SharedPrefManager

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        app_version.text = getString(R.string.app_version) + " " + BuildConfig.VERSION_NAME

        Handler(Looper.myLooper()!!).postDelayed({
            if (SharedPrefManager.getInstance(this).isLoggedIn) {
                val intent = Intent(applicationContext, DashboardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                val intent = Intent(applicationContext, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            }
        }, 2000)
    }
}